const functions = require("firebase-functions");
const nodemailer = require("nodemailer");

const fs = require("fs");
const path = require('path');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp()

const senderAddress = "mailer@chooselifechurch.com";
const mailTransport = nodemailer.createTransport({
    host: "smtp.office365.com",
    port: 587,
    secure: false,
    auth: {
        user: senderAddress,
        pass: "Ch00s3L!f3m@1l"
    }
});

exports.mailFxn = functions.firestore.document('responses/{doc}').onCreate((snapshot, context) => {
    const response = snapshot.data();
    const eventId = context.eventId;
    console.log(`Snapshot Data: ${JSON.stringify(response)}`);
    console.log(`Event Id: ${eventId}`);

    const fullName = `${response.name} ${response.surname}`;
    // const qrEncoded = `name=${name},id=${response.id},eventId=${eventId}`;
    const qrEncoded = `name=${fullName},id=${response.id_number},cell=${response.cellphone_number},service-date=${response.service_date}`;
    const UNIQUE_QR = `https://chart.googleapis.com/chart?chs=300x300&cht=qr&chld=H&chl=${qrEncoded}`;
    console.log(UNIQUE_QR);
    const filePath = path.join(__dirname, `templates/${response.template}`);

    fs.readFile(filePath, { encoding: 'utf-8' }, (err , data) => {
        if (err) { console.log(err); }

        data = data.toString();
        data = data.replace(/##UNIQUE_NAME/g, response.name);
        data = data.replace(/##UNIQUE_QR/g, UNIQUE_QR);

        var mailOptions = {
            from: '"Service Registration" <'+ senderAddress +'>',
            to: response.email,
            subject: 'Thank You For Registering',
            html: data // html body
        };

        try {
            mailTransport.sendMail(mailOptions);
        } catch (error) {
            console.error('There was an error while sending the email:', error);
            // errorEmails = functions.database.ref(`/emailError/${makerID}`).set({
            //   email: makers.email
            // })
        }

        return console.log(
            `Sending mail to ${fullName} @ ${response.email}`
        );
    });

    return null;
});


    //To deploy firebase deploy --only functions:mailFxn



// exports.genderCountFxn = functions.database
//     .ref("/atEvent/{date}/{id}")
//     .onCreate((snapshot, context) => {
//         // Grab the current value of what was written to the Realtime Database.
//         const Visitor = snapshot.val();
//         console.log("Inside Visitor Fxn");
//         console.log(Visitor);
//
//         console.log(Visitor.name);
//
//         //---------------------------------------------------
//         //Working gender counter
//         const genderCountersRef = admin
//             .database()
//             .ref(`count/${Visitor.gender}`);
//
//         return genderCountersRef
//             .transaction(counter_value => {
//                 return (counter_value || 0) + 1;
//             })
//
//     });
//To deploy firebase deploy --only functions:genderCountFxn



// exports.overallCountFxn = functions.database
//     .ref("/atEvent/{date}/{id}")
//     .onCreate((snapshot, context) => {
//         const Visitor = snapshot.val();
//
//         const overallCountersRef = admin
//             .database()
//             .ref(`count/overAllTotal`);
//
//         return overallCountersRef
//             .transaction(counter_value => {
//                 return (counter_value || 0) + 1;
//             })
//     });
//To deploy firebase deploy --only functions:overallCountFxn
